#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
flake8 extension to enforce basic napoleon docstring format
"""

import ast
import re

from . import errors, resources


class NapoleonPlugin(object):
    """
    flake8 extension to enforce basic napoleon docstring format
    """
    name = 'NapoleonPlugin'
    version = '1.0.1'
    off_by_default = True

    def __init__(self, tree):
        """
        Init for NapoleonPlugin

        Args:
            tree (ast): Ast tree object
        """
        self.tree = tree

    def run(self):
        """
        Check if function have docstring
        Check if all function params are cover in docstring
        Check if return in cover by docstring
        Check if raise is cover by docstring
        Check if Keyword Args exists if **kwargs in function
        Check docstring indent
        """
        skip_pytest = False
        retrun_ = "Returns:"
        kwargs_ = "Keyword Args:"
        raises_ = "Raises:"
        funcs = [i for i in self.tree.body if isinstance(i, ast.FunctionDef)]
        classes = [i for i in self.tree.body if isinstance(i, ast.ClassDef)]
        classes_funcs = list()
        for clss in classes:
            classes_funcs.extend(
                [i for i in clss.body if isinstance(i, ast.FunctionDef)]
            )

        for f in funcs + classes_funcs:
            f_decorator = f.decorator_list
            for deco in f_decorator:
                #  Don't check pytest fixture functions
                try:
                    d_fixture = deco.func.attr == "fixture"
                    d_pytest = deco.func.value.id == "pytest"
                    if d_fixture and d_pytest:
                        skip_pytest = True
                except AttributeError:
                    pass

            if skip_pytest:
                skip_pytest = False
                continue

            f_name = f.name
            #  Skip tests functions
            if f_name.startswith("test_"):
                continue

            f_column = f.col_offset
            docs = ast.get_docstring(f, clean=False)
            args_ = [i.id for i in f.args.args if hasattr(i, "id")]
            return_ = [i for i in f.body if isinstance(i, ast.Return)]
            raise_ = [i for i in f.body if isinstance(i, ast.Raise)]
            kwarg_ = f.args.kwarg
            line_num = f.lineno
            if not docs:
                yield(
                    line_num, f_column, errors.NP001.format(f_name=f_name),
                    self.name
                )

            elif re.findall(r':.*:', docs):
                yield (
                    line_num, f_column, errors.NP010.format(f_name=f_name),
                    self.name
                )
            else:
                for ar in args_:
                    if ar == "self":
                        continue

                    arg_match = "{arg_} \(.*\):".format(arg_=ar)
                    if not re.findall(arg_match, docs):
                        yield(
                            line_num, f_column, errors.NP002.format(
                                f_name=f_name, arg_=ar
                            ),
                            self.name
                        )
                    else:
                        res, indent = resources.check_indent(
                            val=arg_match, indent=8, column=f_column, docs=docs
                        )
                        if not res:
                            yield(
                                line_num, f_column, errors.NP009.format(
                                    f_name=f_name, arg_=ar, indent=indent
                                ), self.name
                            )

                if return_:
                    if retrun_ not in docs:
                        yield(
                            line_num, f_column, errors.NP003.format(
                                f_name=f_name
                            ),
                            self.name
                        )
                    else:
                        res, indent = resources.check_indent(
                            val=retrun_, indent=4, column=f_column, docs=docs
                        )
                        if not res:
                            yield(
                                line_num, f_column, errors.NP006.format(
                                    f_name=f_name, indent=indent
                                ), self.name
                            )

                if kwarg_:
                    if kwargs_ not in docs:
                        yield(
                            line_num, f_column, errors.NP004.format(
                                f_name=f_name
                            ),
                            self.name
                        )
                    else:
                        res, indent = resources.check_indent(
                            val=kwargs_, indent=4, column=f_column,
                            docs=docs
                        )
                        if not res:
                            yield(
                                line_num, f_column, errors.NP008.format(
                                    f_name=f_name, indent=indent
                                ), self.name
                            )

                if raise_:
                    if raises_ not in docs:
                        yield(
                            line_num, f_column, errors.NP005.format(
                                f_name=f_name
                            ),
                            self.name
                        )
                    else:
                        res, indent = resources.check_indent(
                            val=raises_, indent=4, column=f_column, docs=docs
                        )
                        if not res:
                            yield(
                                line_num, f_column, errors.NP007.format(
                                    f_name=f_name, indent=indent
                                ), self.name
                            )
