#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Resource file
"""

import re


def check_indent(val, indent, column, docs):
    """
    Check if val indent is correct

    Args:
        val (str): Value to check
        indent (int): Right indent
        column (int): Start column to check the indent from
        docs (str): The function docstring

    Returns:
        tuple: True/False and current indent
    """
    val_line = re.findall(r'.*{val}'.format(val=val), docs)
    val_line = val_line[0] if val_line else ""
    val_indent = re.findall(r'^ +', val_line)
    val_indent = len(val_indent[0]) if val_indent else 0
    return val_indent - column == indent, val_indent
