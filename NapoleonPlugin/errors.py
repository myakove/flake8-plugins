#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Errors file
"""

NP001 = "NP001: [{f_name}], Docstring is missing"
NP002 = "NP002: [{f_name}], Arg {arg_} is missing in docstring"
NP003 = "NP003: [{f_name}], Returns: is missing in docstring"
NP004 = "NP004: [{f_name}], Keyword Args: is missing in docstring"
NP005 = "NP005: [{f_name}], Raises: is missing in docstring"
NP006 = "NP006: [{f_name}], Returns: indent incorrect ({indent})"
NP007 = "NP007: [{f_name}], Raises: indent incorrect ({indent})"
NP008 = "NP008: [{f_name}], Keyword Args: indent incorrect ({indent})"
NP009 = "NP009: [{f_name}], Arg {arg_}: indent incorrect ({indent})"
NP010 = "NP010: [{f_name}], incorrect docstring format"
