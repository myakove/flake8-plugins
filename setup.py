#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Setup for flake8 napoleon plugin
"""

from __future__ import with_statement
import setuptools

requires = ["flake8 > 3.0.0"]

setuptools.setup(
    name="NapoleonPlugin",
    license="MIT",
    version="0.1",
    description="flake8 extension to enforce basic napoleon docstring format",
    author="Meni Yakove",
    author_email="myakove@gmail.com",
    url="https://bitbucket.org/myakove/flake8-plugins",
    packages=["NapoleonPlugin"],
    install_requires=requires,
    entry_points={
        'flake8.extension': [
            'NP = NapoleonPlugin:NapoleonPlugin',
        ],
    },
    classifiers=[
        "Framework :: Flake8",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: Quality Assurance",
    ],
)
